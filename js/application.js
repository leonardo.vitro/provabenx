const csv = require('csv-parser');
const fastcsv = require('fast-csv');
const fs = require('fs');
let file;
const results = [];

exports.loadFile = function(filename){
    file = filename;
    return new Promise((resolve, reject) => {
        fs.createReadStream(filename)
        .pipe(csv( {headers : ['from','to','cost']}))
        .on('data', (data) => {
            results.push(data);
        })
        .on('end', resolve)
        .on('error', reject);
    });
}

exports.save = function(item){
    let indexOf = results.findIndex(i => i.from === item.from && i.to === item.to);
    if(indexOf == -1){
        results.push(item);
    }else{
        results[indexOf] = item;
    }
    writeCSV();
}

exports.delete = function(item){
    let indexOf = results.findIndex(i => i.from === item.from && i.to === item.to);
    if(indexOf > -1){
        results.splice(indexOf, 1);
    }
    writeCSV();
}

exports.data = function(){
    return results;
}

let routes = [];
exports.findBestRoute = function(item){
    let route = null;
    if(route==null){
        route = {
            error : true,
            message : "Route not found"
        }
    }
    return route;
}

function findLevel(from, to){

    result = results.filter(function (row) { return row.from === from; });
    for(let index = 0 ; index < result.length ; index++){

        let indexOf = routes.findIndex(i => i.key === `${result[index].from}-${result[index].to}`);
        if(indexOf == -1){
            var reg = {
                key : `${result[index].from}-${result[index].to}`,
                route : [result[index]]
            }
            routes.push(reg);
            console.log(reg);
        }else{
            routes[indexOf].route.push(result[index]);
            console.log(routes[indexOf]);
        }

        if(from !== result[index].to && itemOriginal.to !== result[index].to){
            findLevel(itemOriginal, result[index]);
        }

    }
}

function writeCSV(){
    const ws = fs.createWriteStream(file);
    fastcsv
        .write(results, { headers: false })
        .pipe(ws);
}