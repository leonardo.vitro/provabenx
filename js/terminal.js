const application = require('./application');
const readline = require("readline");

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

exports.startTerminal = function(){
    console.log("Terminal started.\n");
    
    rl.on("close", function() {
        console.log("\nThank's for using our services!");
        process.exit(0);
    });

    findRoute();

}

function findRoute(){
    rl.question("Sample of input: GRU-CGD\n"+
                "Please enter the route? (or enter 'exit'): ", function(input) {

        if(input === 'exit'){
            rl.close();
            return;
        }

        var bestRoute = application.findBestRoute();
        if(bestRoute!=null && bestRoute.error == false){
            console.log(`Best route: ${bestRoute.itens} > ${bestRoute.price}`);
        }else{
            console.log(`Sample of input: GRU-CGD`);
            console.log(`Best route: ${bestRoute.message}`);
        }

        findRoute();

    });
}
