exports.help = function(){
    console.log(
        'Benx - Travel assistant\n'+
        'Sample: \n'+
        '   node start.js <file-name>.csv -c \n'+
        '---------------------------------------------\n'+
        'Commands: \n'+
        '   -c | -C : For console mode.\n'+
        '   -s | -S : For server mode.\n'+
        '   -h | -H : For showing this help.\n'+
        '---------------------------------------------\n'+
        'Thank\'s for using our service!'
    );
}