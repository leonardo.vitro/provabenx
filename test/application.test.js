const application = require('../js/application');

test('Testing loadFile', () => {
    const parans = process.argv.slice(2);
    expect(application.loadFile(parans[0])).toBe('Teste');
});