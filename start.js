const server = require('./js/server');
const terminal = require('./js/terminal');
const about = require('./js/about');
const application = require('./js/application');

const parans = process.argv.slice(2);
let fileName;
let serverMode = "";

if(parans.length==0)
    about.help();
else{
    for(var i = 0 ; i < parans.length ; i++){
        const item = parans[i];
        if(item.toLowerCase().endsWith(".csv"))
            fileName = item;
        else if(item.toLowerCase() == "-t" || item.toLowerCase() == "-s")
            serverMode = item.toLowerCase();
        else if(item.toLowerCase() == "-h"){
            serverMode = '';
            about.help();
            return;
        }
    }
}

if(fileName){

    console.log(`Input file: ${fileName}`);
    application.loadFile(fileName).then( () => {

        if(serverMode.toLowerCase() == "-t"){
            terminal.startTerminal();
        }else if(serverMode.toLowerCase() == "-s"){
            server.startServer();
        }

    });

}else{
    about.help();
}

