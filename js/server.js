const application = require('./application');
const express = require('express');
const app = express();

var bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

exports.startServer = function(){

    const hostname = '127.0.0.1';
    const port = 3000;
    
    app.get('/bestroute', function(req, res) {
        res.json(application.findBestRoute(req.body));
    });

    app.get('/', function(req, res) {
        res.json(application.data());
    });

    app.post('/', function(req, res) {
        application.save(req.body);
        res.end();
    });

    app.delete('/', function(req, res) {
        application.delete(req.body);
        res.end();
    });    
    
    app.listen(port, function() {
        console.log(`Server started at http://${hostname}:${port}/`);
    });

}